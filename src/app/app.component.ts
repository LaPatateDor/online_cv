import { Component, ViewChild } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'online_cv';

  @ViewChild('sidenav') sidenav: any;
  constructor() { }

  menuToggle() { 
    this.sidenav.toggle();
  }

}



if (typeof Worker !== 'undefined') {
  // Create a new
  const worker = new Worker(new URL('./app.worker', import.meta.url));
  worker.onmessage = ({ data }) => {
    console.log(`page got message: ${data}`);
  };
  worker.postMessage('hello');
} else {
  console.log("WEB WORKER NOT SUPPORTED");
  // Web Workers are not supported in this environment.
  // You should add a fallback so that your program still executes correctly.
}