import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LazyloadedRoutingModule } from './lazyloaded-routing.module';
import { LazyloadedComponent } from './lazyloaded.component';


@NgModule({
  declarations: [
    LazyloadedComponent
  ],
  imports: [
    CommonModule,
    LazyloadedRoutingModule
  ]
})
export class LazyloadedModule { }
